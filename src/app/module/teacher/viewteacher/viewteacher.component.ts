import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Teacher } from "../teacher";
import { TeacherService } from '../teacher.service';
import { ThrowStmt } from '@angular/compiler';
export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}



@Component({
  selector: 'app-viewteacher',
  templateUrl: './viewteacher.component.html',
  styleUrls: ['./viewteacher.component.css']
})
export class ViewteacherComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'telephone','action'];
  dataSource: MatTableDataSource<Teacher>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private teacherService : TeacherService) {

  }

  ngOnInit() {
      this.viewdata();
  }
  deletedata(id)
  {
    this.teacherService.deleteTeacher(id).subscribe((res)=>{
      this.ngOnInit();
    })
  }
  viewdata()
  {
    this.teacherService.getTeacher().subscribe((res)=>{
      this.dataSource= new MatTableDataSource(res);
      this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

