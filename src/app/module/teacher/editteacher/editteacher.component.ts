import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../teacher.service';
import { ActivatedRoute, RouterLink, Router } from '@angular/router';
import { Teacher } from '../teacher';

@Component({
  selector: 'app-editteacher',
  templateUrl: './editteacher.component.html',
  styleUrls: ['./editteacher.component.css']
})

export class EditteacherComponent implements OnInit {
  getid:String;
  constructor(private teacherService:TeacherService , private actroute:ActivatedRoute,private route:Router) 
  {
    this.getid= this.actroute.snapshot.params.id;
  }
  Selected:Teacher={_id:null,tname:null,ttelephone:null};
  submit(f)
  {
    this.teacherService.editTeacher(this.getid,f.value).subscribe((res)=>{
      this.route.navigate(['teacher/view'])
    })
  }
  ngOnInit(): void {
    this.teacherService.getaTeacher(this.getid).subscribe((res)=>{
      this.Selected= res;
    })
  }

}
