import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormControlName, Validators } from '@angular/forms';
import { TeacherService } from "../teacher.service";


@Component({
  selector: 'app-createteacher',
  templateUrl: './createteacher.component.html',
  styleUrls: ['./createteacher.component.css']
})
export class CreateteacherComponent implements OnInit {
  form = new FormGroup({
    tname : new FormControl('',Validators.required),
    ttelephone : new FormControl('',Validators.required)
  });

  submit(f)
  {
      this.teacherService.addteacher(this.form.value).subscribe((res)=>{
      });
      console.log(f);
  }
  constructor(private teacherService:TeacherService) { }

  ngOnInit(): void {
  }

}
