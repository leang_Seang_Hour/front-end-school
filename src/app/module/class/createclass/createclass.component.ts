import { Component, OnInit } from '@angular/core';
import { Student } from "../..//student/student";
import { StudentService } from '../../student/student.service';
import { Teacher } from '../../teacher/teacher';
import { TeacherService } from '../../teacher/teacher.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-createclass',
  templateUrl: './createclass.component.html',
  styleUrls: ['./createclass.component.css']
})
export class CreateclassComponent implements OnInit {
  //table
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;


  getstudents:Student[];
  getteachers:Teacher[];
  tphone:String="";
  slastname:String="";
  sgender:String="";
  telephone:String="";
  studentid:String="";

  addstudentintable()
  {
    
  }
  constructor(private teacherService:TeacherService,private studentService:StudentService)
  { 

  }
  autocomplete(event:any):void
  { 
    this.teacherService.getaTeacher(event.value).subscribe(res =>{
      this.tphone = res.ttelephone;
    })
    // console.log(event.value);
  }
  secondAutocomplete(event:any):void
  {
    console.log(event);
    this.studentService.viewaStudent(event.value).subscribe((res)=>{
      this.slastname= res.slastname;
      this.sgender= res.sgender;
      this.telephone = res.stelephone;
      this.studentid = res._id;
    });
  }
  ngOnInit(): void {
    this.studentService.viewdataStudent().subscribe((res)=>{
      this.getstudents=res;
    });
    this.teacherService.getTeacher().subscribe((res)=>{
      this.getteachers=res;
    })
  }

}
