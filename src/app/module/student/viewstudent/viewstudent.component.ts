import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Student } from "../student";
import { StudentService } from '../student.service';
/** Constants used to fill up our data base. */

@Component({
  selector: 'app-viewstudent',
  templateUrl: './viewstudent.component.html',
  styleUrls: ['./viewstudent.component.css']
})
export class ViewstudentComponent implements OnInit {
  displayedColumns: string[] = ['firstname', 'lastname', 'gender', 'telephone','address','action'];
  dataSource: MatTableDataSource<Student>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private studentService:StudentService) {
  }

  ngOnInit() {
    this.getinfo();
    
  }
  deletedata(deleteid)
  {
    console.log(deleteid);
    this.studentService.deleteStudent(deleteid).subscribe((res)=>{
      this.ngOnInit();
    })
  }
  getinfo()
  {
    this.studentService.viewdataStudent().subscribe(res=>{
      this.dataSource=new MatTableDataSource(res);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}