import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultComponent } from './layout/default/default.component';
import { CreateclassComponent } from './module/class/createclass/createclass.component';
import { ViewclassComponent } from './module/class/viewclass/viewclass.component';
import { EditclassComponent } from './module/class/editclass/editclass.component';
import { CreatestudentComponent } from './module/student/createstudent/createstudent.component';
import { EditstudentComponent } from './module/student/editstudent/editstudent.component';
import { ViewstudentComponent } from './module/student/viewstudent/viewstudent.component';
import { CreateteacherComponent } from './module/teacher/createteacher/createteacher.component';
import { ViewteacherComponent } from './module/teacher/viewteacher/viewteacher.component';
import { EditteacherComponent } from './module/teacher/editteacher/editteacher.component';
import { HeaderComponent } from './layout/adminlte/header/header.component';
import { FooterComponent } from './layout/adminlte/footer/footer.component';
import { NavbarComponent } from './layout/adminlte/navbar/navbar.component';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from "@angular/material/input";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {MatAutocompleteModule} from '@angular/material/autocomplete'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

   
@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    CreateclassComponent,
    ViewclassComponent,
    EditclassComponent,
    CreatestudentComponent,
    EditstudentComponent,
    ViewstudentComponent,
    CreateteacherComponent,
    ViewteacherComponent,
    EditteacherComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    MatAutocompleteModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
