import { Injectable } from '@angular/core';
import { Teacher } from './teacher';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  URL= 'http://localhost:3000/teacher';
  SelectSteacher:Teacher;
  constructor(private httpclient :HttpClient) { }
  addteacher(teacher:Teacher):Observable<Teacher>{
    return this.httpclient.post<Teacher>(`${this.URL}`,teacher);
  }
  getaTeacher(id:String):Observable<Teacher>
  { 
    return this.httpclient.get<Teacher>( `${this.URL}/${id}`);
  }
  getTeacher():Observable<Teacher[]>
  {
    return this.httpclient.get<Teacher[]>(`${this.URL}`);
  }
  editTeacher(id:String,teacher:Teacher):Observable<Teacher>
  {
    return this.httpclient.put<Teacher>(`${this.URL}/${id}`,teacher);
  }
  deleteTeacher(id:String):Observable<Teacher>
  {
    return this.httpclient.delete<Teacher>(`${this.URL}/${id}`);
  }
}
