import { Injectable } from '@angular/core';
import { Student } from './student';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class StudentService {
  URL= 'http://localhost:3000/student';
  selectstudent:Student;
  constructor(private httpclient:HttpClient) { }
  addStudent(student:Student):Observable<Student>
  {
    return this.httpclient.post<Student>(`${this.URL}`,student);
  }
  viewdataStudent():Observable<Student[]>
  {
    return this.httpclient.get<Student[]>(`${this.URL}`);
  }
  viewaStudent(id:String):Observable<Student>
  {
    return this.httpclient.get<Student>(`${this.URL}/${id}`);
  }
  editStudent(id:String,student:Student):Observable<Student>
  {
    return this.httpclient.put<Student>(`${this.URL}/${id}`,student);
  }
  deleteStudent(id:String):Observable<Student>
  {
    return this.httpclient.delete<Student>(`${this.URL}/${id}`);
  }
}
