import { Component, OnInit } from '@angular/core';
import { Gender } from '../../GlobalClass/gender';
import { Student } from '../student';
import { StudentService } from '../student.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editstudent',
  templateUrl: './editstudent.component.html',
  styleUrls: ['./editstudent.component.css']
})
export class EditstudentComponent implements OnInit {
  Selected:Student={_id:null,sfirstname:null,slastname:null,stelephone:null,sgender:null,saddress:null}
  genders: Gender[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'}
  ];
  getid :String;
  options: string[] = ['Male', 'Female'];
  constructor(private studentService:StudentService,private actroute:ActivatedRoute,private route:Router) { 
    this.getid=this.actroute.snapshot.params.id;
  }
  Submitupdate(Form)
  {
    console.log("Update SUCESS");
    this.studentService.editStudent(this.getid,Form.value).subscribe((res)=>{
      this.route.navigate(['/student/view']);
    });
  }
  ngOnInit(): void {
    this.studentService.viewaStudent(this.getid).subscribe((res)=>{
      this.Selected=res;
      console.log(this.Selected);
    })
  }

}
