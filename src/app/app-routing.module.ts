import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layout/default/default.component';
import { CreateclassComponent } from './module/class/createclass/createclass.component';
import { ViewclassComponent } from './module/class/viewclass/viewclass.component';
import { EditclassComponent } from './module/class/editclass/editclass.component';
import { CreatestudentComponent } from './module/student/createstudent/createstudent.component';
import { ViewstudentComponent } from './module/student/viewstudent/viewstudent.component';
import { EditstudentComponent } from './module/student/editstudent/editstudent.component';
import { CreateteacherComponent } from './module/teacher/createteacher/createteacher.component';
import { ViewteacherComponent } from './module/teacher/viewteacher/viewteacher.component';
import { EditteacherComponent } from './module/teacher/editteacher/editteacher.component';

const routes: Routes = [
  {path:'',component:DefaultComponent,children:[
    //Class
    {path:'class/create',component:CreateclassComponent},
    {path:'class/view',component:ViewclassComponent},
    {path:'class/edit/:id',component:EditclassComponent},
    //Student
    {path:'student/create',component:CreatestudentComponent},
    {path:'student/view',component:ViewstudentComponent},
    {path:'student/edit/:id',component:EditstudentComponent},
    //Teacher
    {path:'teacher/create',component:CreateteacherComponent},
    {path:'teacher/view',component:ViewteacherComponent},
    {path:'teacher/edit/:id',component:EditteacherComponent}
  ]}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
