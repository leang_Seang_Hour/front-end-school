import { Component, OnInit } from '@angular/core';
import { Gender } from "../../GlobalClass/gender";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentService } from "../student.service";
@Component({
  selector: 'app-createstudent',
  templateUrl: './createstudent.component.html',
  styleUrls: ['./createstudent.component.css']
})
export class CreatestudentComponent implements OnInit {
  form= new FormGroup({
    sfirstname: new FormControl('',Validators.required),
    slastname: new FormControl('',Validators.required),
    stelephone : new FormControl('',Validators.required),
    sgender:new FormControl('',Validators.required),
    saddress:new FormControl('',Validators.required),
  });
  genders: Gender[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'}
  ];
  constructor(private studentservice:StudentService) { }

  ngOnInit(): void {
  }
  Submit(f)
  {
    this.studentservice.addStudent(this.form.value).subscribe((res)=>{
      
    })
  }
}
